package com.example.searchinstalledapp;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.searchinstalledapp.adapter.AppAdapter;
import com.example.searchinstalledapp.model.App;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

public class MainActivity extends AppCompatActivity {

    // Controls
    private EditText etSearch;
    private RecyclerView rvApps;

    private AppAdapter appAdapter;
    private List<App> apps;

    private PublishSubject<String> searchPublisher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        setAdapter();

        //
        addSearchPublisher();
    }

    private void addControls() {
        etSearch = findViewById(R.id.etSearch);

        rvApps = findViewById(R.id.rvApps);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvApps.setLayoutManager(layoutManager);
    }
    private void setAdapter() {
        apps = listInstalledAppByName("");
        appAdapter = new AppAdapter(this, apps);
        rvApps.setAdapter(appAdapter);
    }

    private void addSearchPublisher() {
        searchPublisher = PublishSubject.create();
        searchPublisher
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    apps = listInstalledAppByName(s);
                    appAdapter = new AppAdapter(MainActivity.this, apps);
                    rvApps.setAdapter(appAdapter);
                });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchPublisher.onNext(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    // API
    private List<App> listInstalledAppByName(String name) {

        SystemClock.sleep(1000);

        List<App> apps = new ArrayList<>();
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> lst = pm.getInstalledApplications(0);
        for (ApplicationInfo app: lst) {
            String n = pm.getApplicationLabel(app).toString();
            if (n.toLowerCase().contains(name.toLowerCase())) {
                apps.add(new App(n, app.packageName, app.loadIcon(pm)));
            }
        }
        return  apps;
    }
}
