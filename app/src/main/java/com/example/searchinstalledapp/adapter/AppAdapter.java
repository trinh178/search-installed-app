package com.example.searchinstalledapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.searchinstalledapp.R;
import com.example.searchinstalledapp.model.App;

import java.util.List;

public class AppAdapter extends RecyclerView.Adapter<AppAdapter.ConstraintViewHolder> {
    class ConstraintViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivIcon;
        private TextView tvName;
        private TextView tvPackageName;

        public ConstraintViewHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            tvName = itemView.findViewById(R.id.tvName);
            tvPackageName = itemView.findViewById(R.id.tvPackageName);
        }
    }

    private Context context;
    private List<App> apps;

    // Constructor
    public AppAdapter(Context context, List<App> apps) {
        this.context = context;
        this.apps = apps;
    }

    @NonNull
    @Override
    public ConstraintViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_app, viewGroup, false);
        return new ConstraintViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConstraintViewHolder constraintViewHolder, int i) {
        App app = apps.get(i);

        constraintViewHolder.ivIcon.setImageDrawable(app.getIcon());
        constraintViewHolder.tvName.setText(app.getName());
        constraintViewHolder.tvPackageName.setText(app.getPackageName());
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }
}
